
require "enemies/GooGun"

local CatapultClass = class(Actor,
    function(self)
        Actor.init(self)
        local meteorAtlas = love.graphics.newImage("trebuchet.png")
        self.spriteGroup = SpriteGroup(meteorAtlas, 32)
        self.z = 0
        self.diff = 0
    end
)

local function meteorGetBB(self)
    local bb = { }
    bb.min_x = self.x - 64
    bb.max_x = self.x + 64
    bb.min_y = self.y - 64
    bb.max_y = self.y + 64
    return bb
end

local function catapultDamage(self, point_x, point_y, damageAmount)
    --print("HAH: ", point_x, ", ", point_y, "\n")
    if(self.health < 0) then return end
    --self.vel_y = self.vel_y / 0.99
    self.health = self.health - damageAmount
    if self.health <= 0 then
        -- Upon death the sprite is moved some place unreachable. It's impossible to remove sprites
        -- from a sprite group.

        score = score + 10
        --powerup = {x = self.x, y = self.y, index = math.random(1, 3)}
        --table.insert(powerups, powerup)

        -- Set flags for catapult destruct animation
        self.health = -1
        self.time = 30
        self.time2 = 30
        --removeTarget(self)
    end
end

local function makeCatapult()
    local cat = {}
    cat.health = 20
    cat.maxHealth = 20
    cat.y = -64
    cat.x = math.random(0, arenaWidth)
    cat.time = 0
    cat.time2 = -100
    cat.frameRate = 60
    cat.getBB = meteorGetBB
    cat.damage = catapultDamage
    cat.rotation = math.random(-45, 45) / 180 * math.pi
    cat.vel_x = 0
    cat.vel_y = 60
    cat.weapon = AsteroidTosser()
    if math.random(1, 2) == 2 then cat.target = player1
    else cat.target = player2 end
    return cat
end

function CatapultClass:update(dt)
    if (player1.health > 0 or player2.health > 0) then
        self.diff = self.diff + dt
    end
    if math.random() < .02 * dt * (self.diff / 4 + 10) then
        local newCat = makeCatapult()
        self.spriteGroup:addSprite(newCat)
        addTarget(newCat)
    end
    for _,cat in ipairs(self.spriteGroup.sprites) do
        cat.x = cat.x + cat.vel_x * dt
        cat.y = cat.y + cat.vel_y * dt
	if(cat.health > 0) then
	    if (cat.time2 < 30 and cat.time2 + dt * cat.frameRate >= 30) then
	        cat.weapon:shoot(cat.x, cat.y, cat.rotation)
	    end
	end
        cat.time2 = cat.time2 + dt * cat.frameRate
        if(cat.health > 0) then
            if cat.time2 >= 90 then
                cat.time2 = -90
            end
            if (cat.time2 < 0) then
                cat.time = 0
            elseif (cat.time2 > 30) then
                cat.time = 30 - (cat.time2 - 30) / 2
            else
                cat.time = cat.time2
            end

            if (cat.time2 < 30) then
                dir = math.atan2(cat.target.y - cat.y, cat.target.x - cat.x)
                if (math.abs(dir - (cat.rotation + math.pi / 2)) < .1 * dt) then cat.rotation = dir - math.pi / 2
                elseif (cat.rotation + math.pi / 2 < dir) then cat.rotation = cat.rotation + .1 * dt
                else cat.rotation = cat.rotation - .1 * dt end
                cat.x = cat.x - math.cos(cat.rotation + math.pi / 2) * dt * 10
                cat.y = cat.y - math.sin(cat.rotation + math.pi / 2) * dt * 10
            end
        else
            -- death sequence
            cat.time = cat.time2
            if (cat.time2 >= 78) then
                if(cat.x > -1000) then
                    powerup = {x = cat.x, y = cat.y, index = math.random(1, 6)}
                    table.insert(powerups, powerup)
                    removeTarget(cat)
                    cat.x = -1000
        	    cat.y = -1000
        	end
        	cat.time2 = 0
        	cat.time = 0
            end
        end
    end
end

function CatapultClass:draw()
    self.spriteGroup:draw()
    --[[
    for _,cat in ipairs(self.spriteGroup.sprites) do
        hpRatio = cat.health / cat.maxHealth
        barX = uiHPBarSm:getWidth() / 2 - hpRatio * uiHPBarSm:getWidth() / 2
        uiquad = love.graphics.newQuad(barX, 0, uiBorder:getWidth(), uiBorder:getHeight(), uiBorder:getWidth(), uiBorder:getHeight())
        love.graphics.drawq(uiBorder, uiquad, cat.x - 2 - uiHPBarSm:getWidth() * 0.5, cat.y - 22)
        
        uiquad2 = love.graphics.newQuad(barX, 0, uiHPBarSm:getWidth() / 2, uiHPBarSm:getHeight(), uiHPBarSm:getWidth(), uiHPBarSm:getHeight())
        love.graphics.drawq(uiHPBarSm, uiquad2, cat.x - uiHPBarSm:getWidth() * 0.5, cat.y - 20)
        
    end
    ]]
end

return CatapultClass
