require "Projectile"

-- Definition of the laser beam projectile class
local LaserBeam = class(Actor,
    function(self, x, y)
        Actor.init(self)
        self.x = x
        self.y = y
        self.z = 1
        self.vel_x = 0
        self.vel_y = -1000
        self.tim = 0
    end
)

function LaserBeam:update(dt)
    self.x = self.x + self.vel_x * dt
    self.y = self.y + self.vel_y * dt
    self.tim = self.tim + dt * 2

    -- Remove this laser beam when it gets to far up.
    if self.y < -100 then
        self:remove()
    end
    tran = (1 - self.tim / 6)
    size = self.tim + .1
    damage = self.owner.player.pDam * tran * size * dt * 4
    if damageTarget(self.x, self.y, damage) then
        self.tim = self.tim + dt
    end
end

function  LaserBeam:draw()
    fire = self.im
    tran = 255 * (1 - self.tim / 6)
    if (tran < 0) then tran = 0 end
    love.graphics.setColor(self.dark, self.dark, self.dark, tran)
    fquad = love.graphics.newQuad(0, 0, fire:getWidth(), fire:getHeight(), fire:getWidth(), fire:getHeight())
    size = self.size * (self.tim + .1)
    if (size > self.size) then size = self.size end
    size = size * self.owner.player.pDam
    love.graphics.drawq(fire, fquad, self.x - size / 2, self.y - size / 2, self.rot, size)
    love.graphics.setColor(255, 255, 255, 255)
    --love.graphics.setColor(255, 0, 0, 255)
    --love.graphics.rectangle("fill", self.x - 5, self.y - 5, 10, 10)
    --love.graphics.setColor(255, 255, 255, 255)
end
---

-- Definition of the laser weapon class
local LaserClass = class(Actor,
    function(self)
        Actor.init(self)
        self.cooloff = 0
    end
)

LaserClass.weaponOrder = 0
LaserClass.weaponName = "Laser++"
LaserClass.weaponColor = { 255, 0, 0 }
fire1 = love.graphics.newImage("sprites/fires/fire1.png")
fire2 = love.graphics.newImage("sprites/fires/fire2.png")

local kd = love.keyboard.isDown -- A small shortcut to this crucial functino

function LaserClass:update(dt)
    if self.player.health > 0 and kd(self.player.kshoot) then
        self.cooloff = self.cooloff + dt
        while self.cooloff >= 0 do
            if(self.player.SP < 1) then
            	return
            end
            self.player.SP = self.player.SP - 1
            local laser = LaserBeam(self.player.x, self.player.y - self.player.height / 2 - 10)
            laser.vel_x = math.random(-100, 100) + self.player.xvel
            laser.vel_y = math.random(-500, -200) + self.player.yvel
            laser.owner = self
            if (math.random(1, 2) == 2) then laser.im = fire2 else laser.im = fire1 end
            laser.size = math.random(50, 70) / 60
            laser.rot = math.random(-45, 45) / 180 * math.pi
            laser.dark = math.random(180, 250)
            addActor(laser)
            laser:update(self.cooloff)
            self.cooloff = self.cooloff - self.player.pRate
        end
    else
        self.cooloff = 0
    end
end

return LaserClass
